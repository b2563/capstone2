const Product = require("../models/Product.js");

// Create Product
module.exports.addProduct = (product) => {

	let newProduct = new Product({
		name : product.name,
		description : product.description,
		price : product.price

	});

	return newProduct.save().then((result, err) => {

		// Prduct creation successful
		if(err) {

			return false;

		// Product creation failed
		} else {

			return true;

		};
	});
};

// Retrieve all Products
module.exports.getAllProducts = () => {

	return Product.find({}).then(result => {

		return result;

	});
};

// Retrieve all active product
module.exports.getActiveProducts = (req, res) => {
	
	return Product.find({isActive: true}).then(result => {

		return result;

	})
};

// Retrieve single product
module.exports.getProducts = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {

		return result;

	})
};

// Update Product information
module.exports.updateProduct = (product, paramsId) => {
	let updatedProduct = {
		name: product.name,
		description: product.description,
		price: product.price
	}
	return Product.findByIdAndUpdate(paramsId.productId, updatedProduct).then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;

		}
	})
}

// Archive Product
module.exports.archiveProduct = (reqParams) =>{

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		
		if (error) {

			return false;

		
		} else {

			return true;

		}

	})

}


