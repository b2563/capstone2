const User = require("../models/User.js");
const Order = require("../models/Order.js");
const Product = require("../models/Product.js");
const bcrypt = require("bcrypt");
const auth = require('../auth');

// User registration
module.exports.registerUser = (requestBody) => {
	let newUser = new User({
		firstName: requestBody.firstName,
		lastName: requestBody.lastName, 
		email: requestBody.email,
		password: bcrypt.hashSync(requestBody.password, 10)
	})

    return newUser.save().then((user, err) => {

        if(err) {

            return false;

        } else {

			return true;

		}
	})
};

// User Authentication
module.exports.authenticateUser = (requestBody) => {

    return User.findOne({email: requestBody.email}).then(result => {

        if(result == null) {

			return false;

        } else {

            const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)

            if(isPasswordCorrect) {

                return {access: auth.createAccessToken(result)}

            } else {

				return false;
			}
		}
	})
};







// Set user as Admin
module.exports.userSetAdmin = (req,res) => {
	let updateAdmin = {
		isAdmin: true
	}
	User.findByIdAndUpdate(req.params.id,updateAdmin,{new:true})
	.then(result => {return res.send({message:"Admin Credentials: ", email: result.email, password: result.password='******'})})
};

// Retrive user details
module.exports.userDetails = (req,res) => {
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

/* module.exports.allUsers = (req,res) => {
	User.find({isAdmin: false})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}; */

module.exports.getProfile = (data) => {

	
	return User.findById(data.userId).then(result => {

		
		result.password = "";

		
		return result;

	});

};

module.exports.createOrder = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.orderList.push({productId: data.productId});

		return user.save().then((ordered, err) => {

			if(err) {

				return false;

			} else {

				return true

			}
		})
	});

	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.orderList.push({userId: data.userId});

		return product.save().then((order, err) => {

			if(err) {

				return false;

			} else {

				return true;

			}
		})
	});

	if(isUserUpdated && isProductUpdated) {

		return true;

	} else {

		return false;

	}
}

module.exports.getAllUsers = () => {

	return User.find({}).then(result => {

		return result;

	});
};


