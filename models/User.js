const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email:{
		type: String,
		required: [true, "Email is required."]
	},

	password: {
		type: String,
		required: [true, "Password is required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	order: [
		{
			orderId : {
				type : String,
				required : [true, "Order ID is required!"]
			},
			orderedOn : {
				type : Date,
				default : new Date()
			},
			status : {
				type : String,
				default : "Ordered"
			}

		}
	]
})

module.exports = mongoose.model("User", userSchema)
