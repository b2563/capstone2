const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const orderControllers = require("../controllers/orderControllers.js");
const auth = require("../auth.js");

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {

	userControllers.checkIfEmailExists(req.body).then(resultFromController => res.send(resultFromController));

});

// Route for user registration
router.post("/register", (req, res) => {

	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));

});

// Route for user authentication
router.post("/login", (req, res) => {

	userControllers.authenticateUser(req.body).then(resultFromController => res.send(resultFromController));

});

router.get("/all", (req, res) => {

	userControllers.getAllUsers().then(resultFromController => res.send(resultFromController));

});



router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	
	userControllers.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));

});


router.post("/order", auth.verify, (req, res) => {

	const data ={
		
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}

	userControllers.createOrder(data).then(resultFromController => res.send(resultFromController));
})

module.exports = router

