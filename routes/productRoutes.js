const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js");
const auth = require("../auth");

router.post("/create", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin) {

		productControllers.addProduct(data.product).then(resultFromController => res.send(resultFromController));

	} else {

		res.send(false);

	}
});


router.get("/all", (req, res) => {

	productControllers.getAllProducts().then(resultFromController => res.send(resultFromController));

})

// Route for retrieving all active products
router.get("/active", (req, res) => {

	productControllers.getActiveProducts().then(resultFromController => res.send(resultFromController));

})

// Route for retrieving a specific product
router.get("/:productId", (req, res) => {

	productControllers.getProducts(req.params).then(resultFromController => res.send(resultFromController));

})
// Route for updating a product
router.put("/update/:productId", auth.verify, (req, res) => {

	const data ={
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {

		productControllers.updateProduct(data.course, data.params).then(resultFromController => res.send(resultFromController));

	} else {

		res.send(false);

	}
})

router.put("/archive/:productId", auth.verify, (req, res) => {

	const data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {

		productControllers.archiveProduct(data.params).then(resultFromController => res.send(resultFromController));
	
	} else {

		res.send(false);

	}
});

router.post("/order", auth.verify, (req, res) => {

	const data ={
		
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}

	userController.User(data).then(resultFromController => res.send(resultFromController));
})

module.exports = router;



