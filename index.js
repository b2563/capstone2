const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const orderRoutes = require("./routes/orderRoutes");
const productRoutes = require("./routes/productRoutes");

const app = express();

app.use(express.json());
app.use(cors());
app.use(express.urlencoded({extended: true}));



mongoose.connect("mongodb+srv://admin:admin1234@b256viray.3ercxlm.mongodb.net/B256_EcommerceAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

app.use("/users", userRoutes);
app.use("/orders", orderRoutes);
app.use("/product", productRoutes);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`Were connected to the cloud database`));

app.listen(4000, () => console.log(`API is now onlline on port 4000`));
